/********************************************************************************
** Form generated from reading UI file 'videopuzzleplayer.ui'
**
** Created: Mon Apr 9 17:11:59 2012
**      by: Qt User Interface Compiler version 4.8.0
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_VIDEOPUZZLEPLAYER_H
#define UI_VIDEOPUZZLEPLAYER_H

#include <QtCore/QVariant>
#include <QtGui/QAction>
#include <QtGui/QApplication>
#include <QtGui/QButtonGroup>
#include <QtGui/QFrame>
#include <QtGui/QHBoxLayout>
#include <QtGui/QHeaderView>
#include <QtGui/QMainWindow>
#include <QtGui/QMenu>
#include <QtGui/QMenuBar>
#include <QtGui/QPushButton>
#include <QtGui/QSpacerItem>
#include <QtGui/QStatusBar>
#include <QtGui/QVBoxLayout>
#include <QtGui/QWidget>

QT_BEGIN_NAMESPACE

class Ui_VideoPuzzlePLayer
{
public:
    QAction *actionExit;
    QWidget *centralWidget;
    QWidget *verticalLayoutWidget;
    QVBoxLayout *verticalLayout_2;
    QFrame *frame;
    QHBoxLayout *horizontalLayout_2;
    QPushButton *stopButton;
    QPushButton *playButton;
    QSpacerItem *horizontalSpacer;
    QPushButton *shuffleButton;
    QMenuBar *menuBar;
    QMenu *menuFile;
    QStatusBar *statusBar;

    void setupUi(QMainWindow *VideoPuzzlePLayer)
    {
        if (VideoPuzzlePLayer->objectName().isEmpty())
            VideoPuzzlePLayer->setObjectName(QString::fromUtf8("VideoPuzzlePLayer"));
        VideoPuzzlePLayer->resize(402, 307);
        QSizePolicy sizePolicy(QSizePolicy::Expanding, QSizePolicy::Expanding);
        sizePolicy.setHorizontalStretch(0);
        sizePolicy.setVerticalStretch(0);
        sizePolicy.setHeightForWidth(VideoPuzzlePLayer->sizePolicy().hasHeightForWidth());
        VideoPuzzlePLayer->setSizePolicy(sizePolicy);
        actionExit = new QAction(VideoPuzzlePLayer);
        actionExit->setObjectName(QString::fromUtf8("actionExit"));
        centralWidget = new QWidget(VideoPuzzlePLayer);
        centralWidget->setObjectName(QString::fromUtf8("centralWidget"));
        sizePolicy.setHeightForWidth(centralWidget->sizePolicy().hasHeightForWidth());
        centralWidget->setSizePolicy(sizePolicy);
        verticalLayoutWidget = new QWidget(centralWidget);
        verticalLayoutWidget->setObjectName(QString::fromUtf8("verticalLayoutWidget"));
        verticalLayoutWidget->setGeometry(QRect(0, 0, 401, 251));
        verticalLayout_2 = new QVBoxLayout(verticalLayoutWidget);
        verticalLayout_2->setSpacing(6);
        verticalLayout_2->setContentsMargins(11, 11, 11, 11);
        verticalLayout_2->setObjectName(QString::fromUtf8("verticalLayout_2"));
        verticalLayout_2->setSizeConstraint(QLayout::SetMaximumSize);
        verticalLayout_2->setContentsMargins(0, 0, 0, 0);
        frame = new QFrame(verticalLayoutWidget);
        frame->setObjectName(QString::fromUtf8("frame"));
        sizePolicy.setHeightForWidth(frame->sizePolicy().hasHeightForWidth());
        frame->setSizePolicy(sizePolicy);
        frame->setFrameShape(QFrame::StyledPanel);
        frame->setFrameShadow(QFrame::Raised);

        verticalLayout_2->addWidget(frame);

        horizontalLayout_2 = new QHBoxLayout();
        horizontalLayout_2->setSpacing(6);
        horizontalLayout_2->setObjectName(QString::fromUtf8("horizontalLayout_2"));
        horizontalLayout_2->setSizeConstraint(QLayout::SetDefaultConstraint);
        stopButton = new QPushButton(verticalLayoutWidget);
        stopButton->setObjectName(QString::fromUtf8("stopButton"));

        horizontalLayout_2->addWidget(stopButton);

        playButton = new QPushButton(verticalLayoutWidget);
        playButton->setObjectName(QString::fromUtf8("playButton"));

        horizontalLayout_2->addWidget(playButton);

        horizontalSpacer = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_2->addItem(horizontalSpacer);

        shuffleButton = new QPushButton(verticalLayoutWidget);
        shuffleButton->setObjectName(QString::fromUtf8("shuffleButton"));

        horizontalLayout_2->addWidget(shuffleButton);


        verticalLayout_2->addLayout(horizontalLayout_2);

        VideoPuzzlePLayer->setCentralWidget(centralWidget);
        menuBar = new QMenuBar(VideoPuzzlePLayer);
        menuBar->setObjectName(QString::fromUtf8("menuBar"));
        menuBar->setGeometry(QRect(0, 0, 402, 22));
        menuFile = new QMenu(menuBar);
        menuFile->setObjectName(QString::fromUtf8("menuFile"));
        VideoPuzzlePLayer->setMenuBar(menuBar);
        statusBar = new QStatusBar(VideoPuzzlePLayer);
        statusBar->setObjectName(QString::fromUtf8("statusBar"));
        VideoPuzzlePLayer->setStatusBar(statusBar);

        menuBar->addAction(menuFile->menuAction());

        retranslateUi(VideoPuzzlePLayer);

        QMetaObject::connectSlotsByName(VideoPuzzlePLayer);
    } // setupUi

    void retranslateUi(QMainWindow *VideoPuzzlePLayer)
    {
        VideoPuzzlePLayer->setWindowTitle(QApplication::translate("VideoPuzzlePLayer", "VideoPuzzlePLayer", 0, QApplication::UnicodeUTF8));
        actionExit->setText(QApplication::translate("VideoPuzzlePLayer", "Exit", 0, QApplication::UnicodeUTF8));
        stopButton->setText(QApplication::translate("VideoPuzzlePLayer", "Stop", 0, QApplication::UnicodeUTF8));
        playButton->setText(QApplication::translate("VideoPuzzlePLayer", "Play", 0, QApplication::UnicodeUTF8));
        shuffleButton->setText(QApplication::translate("VideoPuzzlePLayer", "Shuffle", 0, QApplication::UnicodeUTF8));
        menuFile->setTitle(QApplication::translate("VideoPuzzlePLayer", "File", 0, QApplication::UnicodeUTF8));
    } // retranslateUi

};

namespace Ui {
    class VideoPuzzlePLayer: public Ui_VideoPuzzlePLayer {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_VIDEOPUZZLEPLAYER_H
