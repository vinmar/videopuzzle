#ifndef STREAMSETTINGS_H
#define STREAMSETTINGS_H

#include <QDialog>
#include <QAbstractButton>

namespace Ui {
class StreamSettings;
}

class StreamSettings : public QDialog
{
    Q_OBJECT
    
public:
    explicit StreamSettings(QWidget *parent = 0);
    ~StreamSettings();
    
private slots:

    void on_CloseSavebuttonBox_clicked(QAbstractButton *button);

private:
    Ui::StreamSettings *ui;
};

#endif // STREAMSETTINGS_H
