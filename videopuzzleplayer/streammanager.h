#ifndef STREAMMANAGER_H
#define STREAMMANAGER_H

#include <string>
#include <stdlib.h>
#include <cstddef>
#include <vector>
#include <bom.h>
using namespace std;

/*
This class wiil handle the connection to the different streams using Gsteamer
It will also provide the next frame for each stream
*/

class StreamManager
{

private:
        static StreamManager* instance_ptr;
        StreamManager() { _running = false; }
        bool _running;
        std::vector<t_stream> _streamVector;


public:
        ~StreamManager() {}
        static StreamManager* get_instance() {
                if (instance_ptr == NULL) {
                    instance_ptr = new StreamManager();
                }
                return instance_ptr;
        }

        void load();
        void unload();
        void play();
        void stop();
        bool getNextFrameFromStream(int streamId);
        void saveUrls(std::string urls);


        std::string getUrlsAsString();

        const std::vector<t_stream> & getStreamVector(){return _streamVector;}
        std::vector<t_stream> & accessStreamVector(){return _streamVector;}

};

#endif // STREAMMANAGER_H
