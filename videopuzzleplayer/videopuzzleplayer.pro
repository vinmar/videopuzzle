#-------------------------------------------------
#
# Project created by QtCreator 2012-04-09T13:43:48
#
#-------------------------------------------------

QT       += core gui phonon console

TARGET = videopuzzleplayer
TEMPLATE = app


SOURCES += main.cpp\
        videopuzzleplayer.cpp \
    streamsettings.cpp \
    streammanager.cpp \
    buffermanager.cpp \
    gridmanager.cpp

HEADERS  += videopuzzleplayer.h \
    streamsettings.h \
    streammanager.h \
    buffermanager.h \
    gridmanager.h \
    bom.h

FORMS    += videopuzzleplayer.ui \
    streamsettings.ui

OTHER_FILES += \
    streamsurls.txt

unix {
    CONFIG += link_pkgconfig
    PKGCONFIG += gstreamer-0.10
}
