#ifndef VIDEOPUZZLEPLAYER_H
#define VIDEOPUZZLEPLAYER_H

#include <QMainWindow>

namespace Ui {
class VideoPuzzlePLayer;
}

class VideoPuzzlePLayer : public QMainWindow
{
    Q_OBJECT
    
public:
    explicit VideoPuzzlePLayer(QWidget *parent = 0);
    ~VideoPuzzlePLayer();
    
private slots:
    void on_stopButton_clicked();

    void on_playButton_clicked();

    void on_shuffleButton_clicked();

private:
    Ui::VideoPuzzlePLayer *ui;
    void load(void);

};

#endif // VIDEOPUZZLEPLAYER_H
