/********************************************************************************
** Form generated from reading UI file 'streamsettings.ui'
**
** Created: Tue Apr 10 23:41:50 2012
**      by: Qt User Interface Compiler version 4.6.2
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_STREAMSETTINGS_H
#define UI_STREAMSETTINGS_H

#include <QtCore/QVariant>
#include <QtGui/QAction>
#include <QtGui/QApplication>
#include <QtGui/QButtonGroup>
#include <QtGui/QDialog>
#include <QtGui/QDialogButtonBox>
#include <QtGui/QHeaderView>
#include <QtGui/QLabel>
#include <QtGui/QTextEdit>

QT_BEGIN_NAMESPACE

class Ui_StreamSettings
{
public:
    QDialogButtonBox *CloseSavebuttonBox;
    QTextEdit *textEdit;
    QLabel *label;

    void setupUi(QDialog *StreamSettings)
    {
        if (StreamSettings->objectName().isEmpty())
            StreamSettings->setObjectName(QString::fromUtf8("StreamSettings"));
        StreamSettings->resize(433, 298);
        CloseSavebuttonBox = new QDialogButtonBox(StreamSettings);
        CloseSavebuttonBox->setObjectName(QString::fromUtf8("CloseSavebuttonBox"));
        CloseSavebuttonBox->setGeometry(QRect(80, 240, 341, 32));
        CloseSavebuttonBox->setOrientation(Qt::Horizontal);
        CloseSavebuttonBox->setStandardButtons(QDialogButtonBox::Close|QDialogButtonBox::Save);
        textEdit = new QTextEdit(StreamSettings);
        textEdit->setObjectName(QString::fromUtf8("textEdit"));
        textEdit->setGeometry(QRect(20, 40, 401, 191));
        label = new QLabel(StreamSettings);
        label->setObjectName(QString::fromUtf8("label"));
        label->setGeometry(QRect(20, 10, 401, 21));

        retranslateUi(StreamSettings);
        QObject::connect(CloseSavebuttonBox, SIGNAL(accepted()), StreamSettings, SLOT(accept()));
        QObject::connect(CloseSavebuttonBox, SIGNAL(rejected()), StreamSettings, SLOT(reject()));

        QMetaObject::connectSlotsByName(StreamSettings);
    } // setupUi

    void retranslateUi(QDialog *StreamSettings)
    {
        StreamSettings->setWindowTitle(QApplication::translate("StreamSettings", "Dialog", 0, QApplication::UnicodeUTF8));
        label->setText(QApplication::translate("StreamSettings", "Insert the url of the stream to be loaded; one video per row", 0, QApplication::UnicodeUTF8));
    } // retranslateUi

};

namespace Ui {
    class StreamSettings: public Ui_StreamSettings {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_STREAMSETTINGS_H
