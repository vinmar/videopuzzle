/*
This class will handle the frame disposition in to the main widget. It will be in charge to take a corridpondance
among the videos and the pportion of thw main widget that will display it.
*/

#ifndef GRIDMANAGER_H
#define GRIDMANAGER_H

#include "bom.h"


class GridManager
{
private:
        static GridManager* instance_ptr;
        GridManager() {}

        void initGridPos(t_gridPos t){
            t.tl_x = 0;
            t.tl_y = 0;
            t.br_x = 0;
            t.br_y = 0;
        };


public:
        ~GridManager() {}
        static GridManager* get_instance() {
                if (instance_ptr == NULL) {
                    instance_ptr = new GridManager();
                }
                return instance_ptr;
        }

        t_gridPos calculateGridPos(int idVideo, int totalNumberOfVideo, int screenWidth, int screenLength);
        int getVideoIdFromClick(int mouse_x, int mouse_y);



};

#endif // GRIDMANAGER_H
