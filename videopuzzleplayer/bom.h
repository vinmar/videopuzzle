#ifndef BOM_H
#define BOM_H

#include<string>
#include<vector>
#include <QtGui/QWidget>
#include <Phonon/MediaObject>
#include <Phonon/VideoPlayer>

typedef struct gridpos
{
  int tl_x, tl_y;
  int br_x, br_y;
} t_gridPos;




typedef struct stream
{
    int id;
    std::string url;
    QWidget * widget;
    Phonon::MediaObject * media;
    Phonon::VideoPlayer * videoPlayer;
    t_gridPos gridPos;
}t_stream;



#endif // BOM_H
