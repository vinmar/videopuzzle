#include "videopuzzleplayer.h"
#include "ui_videopuzzleplayer.h"
#include "streammanager.h"
#include "gridmanager.h"
#include <QMessageBox>
#include <QDebug>


VideoPuzzlePLayer::VideoPuzzlePLayer(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::VideoPuzzlePLayer)
{
    ui->setupUi(this);

    load();
}


VideoPuzzlePLayer::~VideoPuzzlePLayer()
{
    StreamManager::get_instance()->unload();
    delete ui;
}


void VideoPuzzlePLayer::load()
{


    ui->frame->showMaximized();
    ui->frame->setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Expanding);
    ui->frame->updateGeometry();

    // read the file with the urls
    {
    t_stream ts;
    ts.url = "http://demo.cdn01.rambla.be/bwcheck/bbb-1000.mp4";
    //ts.url = "/home/vincenzo/Videos/tenis0001.avi";
    //ts.url = "../movies/home/bbb-1000.mp4";
    ts.id = 0;

   //create the QWidget and layout it in the grid
    QWidget * widget = new QWidget(ui->frame);
    widget->setObjectName(QString::fromUtf8("widget0"));
    t_gridPos tp = GridManager::get_instance()->calculateGridPos(0, 2, ui->frame->size().width(), ui->frame->size().height());

    widget->setGeometry(QRect(tp.tl_x, tp.tl_y, tp.br_x-tp.tl_x, tp.br_y-tp.tl_y));

    qDebug() << "Stream  " << 0;
    qDebug() << "ui->frame->size().width() is: " << ui->frame->size().width();
    qDebug() << "ui->frame->size().height() is: " << ui->frame->size().height();
    qDebug() << "tl_x is: " << tp.tl_x;
    qDebug() << "tl_y is: " << tp.tl_y;
    qDebug() << "br_x is: " << tp.br_x;
    qDebug() << "br_y is: " << tp.br_y;
    ts.widget = widget;

    //store (and pass the information ) to the stream manager
    StreamManager::get_instance()->accessStreamVector().push_back(ts);
    }

    {
    t_stream ts;
    //ts.url = "/home/vincenzo/Videos/tenis0001.avi";
    ts.url = "http://demo.cdn01.rambla.be/bwcheck/bbb-1000.mp4";
    ts.id = 1;

   //create the QWidget and layout it in the grid
    QWidget * widget = new QWidget(ui->frame);
    widget->setObjectName(QString::fromUtf8("widget1"));
    t_gridPos tp = GridManager::get_instance()->calculateGridPos(1, 2, ui->frame->size().width(), ui->frame->size().height());

    widget->setGeometry(QRect(tp.tl_x, tp.tl_y, tp.br_x-tp.tl_x, tp.br_y-tp.tl_y));

    qDebug() << "Stream  " << 1;
    qDebug() << "ui->frame->size().width() is: " << ui->frame->size().width();
    qDebug() << "ui->frame->size().height() is: " << ui->frame->size().height();
    qDebug() << "tl_x is: " << tp.tl_x;
    qDebug() << "tl_y is: " << tp.tl_y;
    qDebug() << "br_x is: " << tp.br_x;
    qDebug() << "br_y is: " << tp.br_y;
    ts.widget = widget;

    //store (and pass the information ) to the stream manager
    StreamManager::get_instance()->accessStreamVector().push_back(ts);
    }


    StreamManager::get_instance()->load();



}

void VideoPuzzlePLayer::on_stopButton_clicked()
{
    /*
    stop reading streaming video flow
    */
    StreamManager::get_instance()->stop();
}

void VideoPuzzlePLayer::on_playButton_clicked()
{
    /*
    start acquiring streaming video flow
    */

    StreamManager::get_instance()->play();
}

void VideoPuzzlePLayer::on_shuffleButton_clicked()
{
    /*
    shuffle videos position
    */
}
