#include "streammanager.h"
#include "gridmanager.h"
//#include <gst/gst.h>
#include <QDebug>






StreamManager* StreamManager::instance_ptr = NULL;

void StreamManager::load()
{
    for (unsigned int i = 0; i < _streamVector.size(); i++){

        Phonon::MediaObject * media = new Phonon::MediaObject();
        //media->setCurrentSource(Phonon::MediaSource(QString("/home/vincenzo/Videos/tenis0001.avi")));
        media->setCurrentSource(Phonon::MediaSource(QString(_streamVector.at(i).url.c_str())));


        Phonon::VideoPlayer *videoPlayer = new Phonon::VideoPlayer(Phonon::VideoCategory, _streamVector.at(i).widget);


        //set the position
        videoPlayer->setFixedSize(_streamVector.at(i).widget->size());
        videoPlayer->move(_streamVector.at(i).gridPos.tl_x, _streamVector.at(i).gridPos.tl_y);

        //store pointer of created obj for future usage
        _streamVector.at(i).videoPlayer = videoPlayer;
        _streamVector.at(i).media = media;
    }
}

void StreamManager::unload()
{
    
    stop();
    
    for (unsigned int i = 0; i < _streamVector.size(); i++){

        delete _streamVector.at(i).videoPlayer;
        _streamVector.at(i).videoPlayer = NULL;
        delete _streamVector.at(i).media;
        _streamVector.at(i).media = NULL;
    }
    
    
}



void StreamManager::play()
{

    qDebug() << "is running : " << _running;
    qDebug() << "_streamVector.size() : " << _streamVector.size();


    if ( ! _running ){


        for (unsigned int i = 0; i < _streamVector.size(); i++){

            //show
            Q_ASSERT(_streamVector.at(i).videoPlayer != NULL);
            _streamVector.at(i).videoPlayer->show();

            //play
            Q_ASSERT(_streamVector.at(i).media != NULL);
            _streamVector.at(i).videoPlayer->play(_streamVector.at(i).media->currentSource());
        }
    }






/*
    gst_init (NULL,NULL);

        GstElement *bin = gst_pipeline_new ("pipeline");
        g_assert(bin);

        GstElement *testSrc = gst_element_factory_make("videotestsrc", "source");
        g_assert(testSrc);

        GstElement *videoOut = gst_element_factory_make("autovideosink", "video out");
        g_assert(videoOut);

        gst_bin_add_many(GST_BIN(bin), testSrc, videoOut, NULL);
        gst_element_link_many(testSrc, videoOut, NULL);

        gst_element_set_state(GST_ELEMENT(bin), GST_STATE_PLAYING);

*/
    _running = true;
}

bool StreamManager::getNextFrameFromStream(int streamId)
{
    return true;
}

void StreamManager::stop()
{
    if ( _running )
    {

        for ( unsigned int i = 0; i < _streamVector.size(); i++){
            _streamVector.at(i).videoPlayer->stop();
        }

    }
    _running = false;
}

std::string StreamManager::getUrlsAsString()
{
    std::string ulrs;

    return ulrs;
}

void StreamManager::saveUrls(std::string urls)
{
    //todo: finish implementation
}

