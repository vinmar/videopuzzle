#include "gridmanager.h"
#include "streammanager.h"
#include <math.h>

GridManager* GridManager::instance_ptr = NULL;

/*
calculate che grid positon supposing a squared displacement
first video id is 0
*/
t_gridPos GridManager::calculateGridPos(int idVideo, int totalNumberOfVideo, int screenWidth, int screenLength)
{
    t_gridPos t;
    initGridPos(t);

    int numberOfVideoPerSide = (int)sqrt(totalNumberOfVideo);

    if ( numberOfVideoPerSide > 0){

        int span_x = ( idVideo % numberOfVideoPerSide );
        int span_y = idVideo / numberOfVideoPerSide;


        int videoWidth = screenWidth / numberOfVideoPerSide ;
        int videoHeight = screenLength / numberOfVideoPerSide ;

        t.tl_x = span_x * videoWidth;
        t.tl_y = span_y * videoHeight;
        t.br_x = t.tl_x + videoWidth;
        t.br_y = t.tl_y + videoHeight;
    }else{
        //todo: WARNING
    }

    return t;

}


int GridManager::getVideoIdFromClick(int mouse_x, int mouse_y)
{
    int selectedVideoID = -1;
    //loop over all the videos loaded lookig to their grid position
    int numberOfVideo = StreamManager::get_instance()->getStreamVector().size();

    for (int i = 0; i < numberOfVideo || selectedVideoID == -1; i++)
    {
        if (  (StreamManager::get_instance()->getStreamVector().at(i).gridPos.tl_x > mouse_x) && (mouse_x < StreamManager::get_instance()->getStreamVector().at(i).gridPos.br_x) )
        {
            if ( (StreamManager::get_instance()->getStreamVector().at(i).gridPos.tl_y > mouse_y) && (mouse_y < StreamManager::get_instance()->getStreamVector().at(i).gridPos.br_y) )
            {
                selectedVideoID = i;
            }
        }
    }

    return selectedVideoID;
}
